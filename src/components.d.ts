/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */


import { HTMLStencilElement, JSXBase } from '@stencil/core/internal';


export namespace Components {
  interface AppAddPlayer {}
  interface AppCreatePlayer {
    'img': string;
    'index': number;
    'leaderboard': any;
    'name': string;
    'slack': string;
    'wins': number;
  }
  interface AppHeader {}
  interface AppLeaderboard {}
  interface AppRoot {}
  interface TestLeaderboard {}
}

declare global {


  interface HTMLAppAddPlayerElement extends Components.AppAddPlayer, HTMLStencilElement {}
  var HTMLAppAddPlayerElement: {
    prototype: HTMLAppAddPlayerElement;
    new (): HTMLAppAddPlayerElement;
  };

  interface HTMLAppCreatePlayerElement extends Components.AppCreatePlayer, HTMLStencilElement {}
  var HTMLAppCreatePlayerElement: {
    prototype: HTMLAppCreatePlayerElement;
    new (): HTMLAppCreatePlayerElement;
  };

  interface HTMLAppHeaderElement extends Components.AppHeader, HTMLStencilElement {}
  var HTMLAppHeaderElement: {
    prototype: HTMLAppHeaderElement;
    new (): HTMLAppHeaderElement;
  };

  interface HTMLAppLeaderboardElement extends Components.AppLeaderboard, HTMLStencilElement {}
  var HTMLAppLeaderboardElement: {
    prototype: HTMLAppLeaderboardElement;
    new (): HTMLAppLeaderboardElement;
  };

  interface HTMLAppRootElement extends Components.AppRoot, HTMLStencilElement {}
  var HTMLAppRootElement: {
    prototype: HTMLAppRootElement;
    new (): HTMLAppRootElement;
  };

  interface HTMLTestLeaderboardElement extends Components.TestLeaderboard, HTMLStencilElement {}
  var HTMLTestLeaderboardElement: {
    prototype: HTMLTestLeaderboardElement;
    new (): HTMLTestLeaderboardElement;
  };
  interface HTMLElementTagNameMap {
    'app-add-player': HTMLAppAddPlayerElement;
    'app-create-player': HTMLAppCreatePlayerElement;
    'app-header': HTMLAppHeaderElement;
    'app-leaderboard': HTMLAppLeaderboardElement;
    'app-root': HTMLAppRootElement;
    'test-leaderboard': HTMLTestLeaderboardElement;
  }
}

declare namespace LocalJSX {
  interface AppAddPlayer extends JSXBase.HTMLAttributes<HTMLAppAddPlayerElement> {}
  interface AppCreatePlayer extends JSXBase.HTMLAttributes<HTMLAppCreatePlayerElement> {
    'img'?: string;
    'index'?: number;
    'leaderboard'?: any;
    'name'?: string;
    'onUpdateLeaderboard'?: (event: CustomEvent<any>) => void;
    'slack'?: string;
    'wins'?: number;
  }
  interface AppHeader extends JSXBase.HTMLAttributes<HTMLAppHeaderElement> {}
  interface AppLeaderboard extends JSXBase.HTMLAttributes<HTMLAppLeaderboardElement> {}
  interface AppRoot extends JSXBase.HTMLAttributes<HTMLAppRootElement> {}
  interface TestLeaderboard extends JSXBase.HTMLAttributes<HTMLTestLeaderboardElement> {}

  interface IntrinsicElements {
    'app-add-player': AppAddPlayer;
    'app-create-player': AppCreatePlayer;
    'app-header': AppHeader;
    'app-leaderboard': AppLeaderboard;
    'app-root': AppRoot;
    'test-leaderboard': TestLeaderboard;
  }
}

export { LocalJSX as JSX };


declare module "@stencil/core" {
  export namespace JSX {
    interface IntrinsicElements extends LocalJSX.IntrinsicElements {}
  }
}


