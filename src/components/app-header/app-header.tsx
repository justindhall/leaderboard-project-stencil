import {Component, h} from '@stencil/core';

@Component({
  tag: 'app-header',
})
export class AppHeader {

  render() {
    return [
      <ion-list-header>
        <ion-toolbar>
          <ion-title>
            Bingo Night
          </ion-title>
        </ion-toolbar>
      </ion-list-header>
    ]
    }
  }
