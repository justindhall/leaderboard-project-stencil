import {Component, h, State} from '@stencil/core';

@Component({
  tag: 'app-add-player',
})
export class AppAddPlayer {

  @State() value: string;
  @State() selectValue: string;
  @State() secondSelectValue: string;
  @State() avOptions: any[];

  handleSubmit(e) {
    console.log(e.value);
  }

  handleChange(event) {
    this.value = event.target.value;

    if (event.target.validity.typeMismatch) {
      console.log('this element is not valid')
    }
  }

  handleSelect(event) {
    console.log(event.target.value);
    this.selectValue = event.target.value;
  }

  handleSecondSelect(event) {
    console.log(event.target.value);
    this.secondSelectValue = event.target.value;
  }

  render() {
    return [
      <form onSubmit={(e) => this.handleSubmit(e)}>
        <ion-list>
          <ion-item>
            <ion-label position='floating'>Name</ion-label>
            <ion-input></ion-input>
          </ion-item>
          <ion-item>
            <ion-label position='floating'>@Slack</ion-label>
            <ion-input></ion-input>
          </ion-item>
          <ion-item>
            <ion-label position='floating'>Avatar URL</ion-label>
            <ion-input></ion-input>
          </ion-item>
          <ion-item lines="none">
           <ion-button shape="round" slot="end">
             <ion-label>
               Submit
             </ion-label>
           </ion-button>
          </ion-item>
        </ion-list>
      </form>
    ];
  }
}
