import { Component, h } from '@stencil/core';

@Component({
  tag: 'app-root',
  styleUrl: 'app-root.css'
})
export class AppRoot {

  render() {
    return (
      <ion-app>
        <ion-router useHash={false}>
          <ion-route url="/" component="app-leaderboard" />
          <ion-route url="/add" component="app-add-player" />
          <ion-route url="/create" component="app-create-player" />
          <ion-route url="/test" component="test-leaderboard" />
        </ion-router>
        <ion-nav />
      </ion-app>
    );
  }
}
