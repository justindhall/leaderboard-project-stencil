import {Component, h, State} from '@stencil/core';

@Component({
  tag: 'app-leaderboard',
})
export class AppLeaderboard {

  @State() leaderboard = [{name: "Justin", img: "https://images.pexels.com/photos/927022/pexels-photo-927022.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500", slack: "@justin", wins: 10},
    {name: "Evan", img: "https://images.pexels.com/photos/440581/pexels-photo-440581.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&w=500", slack: "@evan", wins: 15},
    {name: "Daniel", img: "https://images.pexels.com/photos/684387/pexels-photo-684387.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500", slack: "@daniel", wins: 7},
    {name: "Ashlyn", img: "https://images.pexels.com/photos/935756/pexels-photo-935756.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&w=500", slack: "@ashlyn", wins: 12},
    {name: "Justin", img: "https://images.pexels.com/photos/927022/pexels-photo-927022.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500", slack: "@justin", wins: 10},
    {name: "Evan", img: "https://images.pexels.com/photos/440581/pexels-photo-440581.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&w=500", slack: "@evan", wins: 15},
    {name: "Daniel", img: "https://images.pexels.com/photos/684387/pexels-photo-684387.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500", slack: "@daniel", wins: 7},
    {name: "Ashlyn", img: "https://images.pexels.com/photos/935756/pexels-photo-935756.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&w=500", slack: "@ashlyn", wins: 12},
    {name: "Justin", img: "https://images.pexels.com/photos/927022/pexels-photo-927022.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500", slack: "@justin", wins: 10},
    {name: "Evan", img: "https://images.pexels.com/photos/440581/pexels-photo-440581.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&w=500", slack: "@evan", wins: 15}
  ];

  @State() adding: boolean = false;

  toggle() {
    this.adding = !this.adding
  }

  renderLeaderboard() {
    return this.leaderboard.map( (player) => {
      return [
        <ion-item>
          <ion-avatar slot="start">
            <ion-img src={player.img}/>
          </ion-avatar>
          <ion-label>{player.name}</ion-label>
          <ion-label>{player.slack}</ion-label>
          <ion-text>{player.wins}</ion-text>
        </ion-item>
      ]
    })
  }

  render() {
    if (this.adding) {
      return [
        <ion-content>
          <app-add-player></app-add-player>,
          <ion-fab vertical="bottom" horizontal="end" slot="fixed">
            <ion-fab-button onClick={() => {
              this.toggle()
            }}>
              <ion-icon name="arrow-round-back"></ion-icon>
            </ion-fab-button>
          </ion-fab>
        </ion-content>
      ]
    } else {
      return [
        <app-header />,
        <ion-content>
          <ion-list>
            {this.renderLeaderboard()}
          </ion-list>
          <ion-fab vertical="bottom" horizontal="end" slot="fixed">
            <ion-fab-button onClick={() => { this.toggle() }}>
              <ion-icon name="person-add"></ion-icon>
            </ion-fab-button>
          </ion-fab>
        </ion-content>
      ]
    }
  }
}
