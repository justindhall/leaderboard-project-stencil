import {Component, h, State, Listen} from '@stencil/core';

@Component({
  tag: 'test-leaderboard',
})
export class TestLeaderboard {

  @State() leaderboard = [{name: "Justin", img: "https://images.pexels.com/photos/927022/pexels-photo-927022.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500", slack: "@justin", wins: 10},
    {name: "Evan", img: "https://images.pexels.com/photos/440581/pexels-photo-440581.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&w=500", slack: "@evan", wins: 15},
    {name: "Daniel", img: "https://images.pexels.com/photos/684387/pexels-photo-684387.jpeg?auto=compress&cs=tinysrgb&dpr=2&w=500", slack: "@daniel", wins: 7},
    {name: "Ashlyn", img: "https://images.pexels.com/photos/935756/pexels-photo-935756.jpeg?auto=format%2Ccompress&cs=tinysrgb&dpr=2&w=500", slack: "@ashlyn", wins: 12}
  ];

  @State() adding: boolean = false;

  @Listen('updateLeaderboard')
    listenForUpdate(event) {
      const copy = [...this.leaderboard];
      copy[event.details.index] = event.deatil.value;
      this.leaderboard = [...copy]
  }

  toggle() {
    this.adding = !this.adding
  }

  renderLeaderboard() {
    return this.leaderboard.map( (player) => {
      return [
        <app-create-player name={player.name} img={player.img} slack={player.slack} wins={player.wins} />
      ]
    })
  }

  render() {
    if (this.adding) {
      return [
        <ion-content>
          <app-create-player></app-create-player>,
          <ion-fab vertical="bottom" horizontal="end" slot="fixed">
            <ion-fab-button onClick={() => {
              this.toggle()
            }}>
              <ion-icon name="arrow-round-back"></ion-icon>
            </ion-fab-button>
          </ion-fab>
        </ion-content>
      ]
    } else {
      return [
        <app-header />,
        <ion-content>
          <ion-list>
            <div>{JSON.stringify(this.leaderboard)}</div>
            {this.renderLeaderboard()}
          </ion-list>
          <ion-fab vertical="bottom" horizontal="end" slot="fixed">
            <ion-fab-button onClick={() => { this.toggle() }}>
              <ion-icon name="person-add"></ion-icon>
            </ion-fab-button>
          </ion-fab>
        </ion-content>
      ]
    }
  }
}
