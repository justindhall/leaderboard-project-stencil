import {Component, EventEmitter, h, Prop, Event} from '@stencil/core';

@Component({
  tag: 'app-create-player',
})
export class AppCreatePlayer {

  @Prop() leaderboard: any;
  @Prop() name: string;
  @Prop() img: string;
  @Prop() slack: string;
  @Prop() wins: number;
  @Prop() index: number;
  @Event() updateLeaderboard: EventEmitter;

  update() {
    this.updateLeaderboard.emit({index: this.index, value: Math.random().toString()})
  }


  render() {
    return [
      <ion-item onClick={() => {this.update() }}>
          <ion-avatar slot="start">
            <ion-img src={this.img}/>
          </ion-avatar>
          <ion-label>{this.name}</ion-label>
          <ion-label>{this.slack}</ion-label>
          <ion-text>{this.wins}</ion-text>
      </ion-item>
    ]
  }
}
